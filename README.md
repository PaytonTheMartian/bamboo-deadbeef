# bamboo-deadbeef

Allows control of the DeaDBeeF music player using Bamboo

# deadbeef.bam

Contains the basic API

Provides: `system.so`

Functions:

```bamboo
deadbeef-pause          - pause music
deadbeef-play           - play music
deadbeef-stop           - stop music
deadbeef-next           - next song
deadbeef-prev           - prev song
deadbeef-random         - random song
deadbeef-quit           - quit deadbeef
deadbeef-toggle-pause   - toggles pause/play
```

## deadbeef-cli.bam

A CLI to control DeaDBeeF from your terminal using the above `deadbeef.bam` API.

This is mostly just an example of how to use the API.
